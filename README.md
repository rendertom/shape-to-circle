![Shape to Circle](/Shape%20to%20Circle.gif)

# Shape to Circle #
Script for After Effects to toggles Fill or Stroke visibility for selected Shape Layers in a composition. If no layers selected, then applies to all layers in active composition.

### Installation: ###
Clone or download this repository and copy **Shape to Circle.jsx** to ScriptUI Panels folder:

* **Windows**: Program Files\Adobe\Adobe After Effects <version>\- Support Files\Scripts
* **Mac OS**: Applications/Adobe After Effects <version>/Scripts

Once Installation is finished run the script in After Effects by clicking Window -> **Shape to Circle**

---------
Developed by Tomas Šinkūnas
www.rendertom.com
---------